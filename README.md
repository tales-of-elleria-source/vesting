# Tales of Elleria Linear Vesting Locker 
`VestingManager.sol` is deployed and used for our dapp available at https://locker.talesofelleria.com/.

Ellerian Prince (Wayne) is the maintainer of this repo.
Please send a DM on Discord to `Ellerian Prince#4509` to report any vulnerabilities.
File an issue if you require clearer documentation on specific items.

<br>Thank you!

## Contract Addresses

Arbitrum Goerli Testnet: `0x495BdB9b4684bDDd27ca80BcC607ebfcE8F1f987`
Arbitrum One Mainnet: `TO BE UPDATED`